﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipesSpawner : MonoBehaviour
{
    [SerializeField] private GameObject pipesPrefab;
    private bool IsTimerStarted { get; set; }

    private float waitTime = 1.5f;

    private const float PIPES_POS_X = 5f;



    public void SetUpSpawnTimer(bool value)
    {
        IsTimerStarted = value;

        if (value)
            StartCoroutine(SpawnLoop());
        else
            StopAllCoroutines();
    }

    public void ClearPipes()
    {
        GameObject[] pipes = GameObject.FindGameObjectsWithTag("Pipes");

        if (pipes.Length > 0)
        {
            foreach (GameObject pipe in pipes)
            {
                Destroy(pipe);
            }
        }
    }


    private void Start()
    {
        IsTimerStarted = false;
    }

    IEnumerator SpawnLoop()
    {
        while(IsTimerStarted)
        {
            Spawn();
            yield return new WaitForSeconds(waitTime);
        }
    }

    private void Spawn()
    {
        var pipesPosY = Random.Range(0.0f, 2.0f);

        var pipes = Instantiate(pipesPrefab, new Vector3(PIPES_POS_X, pipesPosY, 0), Quaternion.identity);
        pipes.transform.SetParent(gameObject.transform.parent);
    }
}
