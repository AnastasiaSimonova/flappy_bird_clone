﻿using UnityEngine;

public class Pipes : MonoBehaviour
{
    private const int LEFT_BORDER = -5;

    private GameManager gameManager;

    private float speed = 2.5f;


    private void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();

    }

    private void Update()
    {
        if (gameManager.IsGameActive)
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);

            if (transform.position.x <= LEFT_BORDER) Destroy(this.gameObject);
        }
    }
}
