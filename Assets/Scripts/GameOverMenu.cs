using UnityEngine;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] private GameObject currentScoreText;
    [SerializeField] private GameObject highScoreText;

    [SerializeField] private ScoreManager scoreManager;


    public void UpdateCurrentScoreText()
    {
        currentScoreText.GetComponent<Text>().text = "Current score: " + Saves.GetCurrentScore().ToString();
    }

    public void UpdateHighScoreText()
    {
        highScoreText.GetComponent<Text>().text = "High score: " + Saves.GetHighScore().ToString();
    }
}
