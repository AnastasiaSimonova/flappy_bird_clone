using UnityEngine;

public class SoundManager : MonoBehaviour
{
	public AudioSource SoundSource;

	public static SoundManager Instance = null;
	
	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else if (Instance != this)
		{
			Destroy(gameObject);
		}

		DontDestroyOnLoad (gameObject);
	}

	public void Play(AudioClip clip)
	{
		SoundSource.clip = clip;
		SoundSource.Play();
	}
}
