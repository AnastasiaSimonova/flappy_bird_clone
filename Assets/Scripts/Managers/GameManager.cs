﻿using UnityEngine;


public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject startMenu;
    [SerializeField] private GameObject gameOverMenu;

    [SerializeField] private Ground ground;
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private Player player;
    [SerializeField] private PipesSpawner pipesSpawner;

    public bool IsGameActive { get; set; }


    public void GameOver()
    {
        scoreManager.UpdateHighScore();

        gameOverMenu.GetComponent<GameOverMenu>().UpdateCurrentScoreText();
        gameOverMenu.GetComponent<GameOverMenu>().UpdateHighScoreText();

        SetGameActive(false);
        gameOverMenu.SetActive(true);

        pipesSpawner.SetUpSpawnTimer(false);
    }

    public void StartGame()
    {
        if (!IsGameActive)
        {
            SetGameActive(true);

            player.SetPlayerActive(true);
            startMenu.SetActive(false);
            pipesSpawner.SetUpSpawnTimer(true);
            gameOverMenu.SetActive(false);

            gameOverMenu.GetComponent<GameOverMenu>().UpdateHighScoreText();
        }
    }

    public void Restart()
    {
        SetGameActive(false);

        scoreManager.ResetCurrentScore();
        player.ResetPosition();
        startMenu.SetActive(true);
        gameOverMenu.SetActive(false);

        pipesSpawner.SetUpSpawnTimer(false);
        pipesSpawner.ClearPipes();
    }


    private void Start()
    {
        SetGameActive(false);
        startMenu.SetActive(true);

        gameOverMenu.GetComponent<GameOverMenu>().UpdateHighScoreText();
    }

    private void SetGameActive(bool value)
    {
        IsGameActive = value;

        scoreManager.gameObject.SetActive(value);
        ground.StartAnim(value);
    }
}
