﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private Text currentScoreText;
    [SerializeField] private Text highScoreText;

    [SerializeField] private AudioClip pointSound;

    public int CurrentScore { get; private set; }
    public int HighScore { get; private set; }


    public void ResetCurrentScore()
    {
        CurrentScore = 0;
        UpdateCurrentScoreText(CurrentScore);
    }

    public void UpdateHighScore()
    {
        if (CurrentScore > HighScore)
        {
            HighScore = CurrentScore;

            UpdateHighScoreText(HighScore);
            Saves.SaveHighScore(HighScore);
        }
    }

    public void IncreaseCurrentScore()
    {
        CurrentScore++;
        UpdateCurrentScoreText(CurrentScore);

        SoundManager.Instance.Play(pointSound);
    }


    private void Start()
    {
        ResetCurrentScore();

        HighScore = Saves.GetHighScore();
        UpdateHighScoreText(HighScore);
    }

    private void UpdateCurrentScoreText(int value)
    {
        currentScoreText.text = value.ToString();
    }

    private void UpdateHighScoreText(int value)
    {
        highScoreText.text = "High score " + value.ToString();
    }
}

