﻿using UnityEngine;

public class Ground : MonoBehaviour
{
    [SerializeField] private Animator anim;


    public void StartAnim(bool value)
    {
        anim.enabled = value;
    }
}