﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private GameManager gameManager;

    [SerializeField] private AudioClip hitSound;
    [SerializeField] private AudioClip wingSound;

    public bool IsDead { get; set; }

    private Rigidbody2D playerBody;

    private Quaternion jumpRotation = Quaternion.Euler(0, 0, 45);
    private Quaternion fallingRotation = Quaternion.Euler(0, 0, -30);

    private const float JUMP_POWER = 900f;



    public void SetPlayerActive(bool value)
    {
        playerBody.bodyType = value ? RigidbodyType2D.Dynamic : RigidbodyType2D.Static;

        IsDead = !value;
        enabled = value;
        GetComponent<Animator>().enabled = value;
    }

    public void ResetPosition()
    {
        playerBody.transform.position = new Vector3(0, 0, 0);
        playerBody.transform.rotation = Quaternion.identity;
    }


    private void Start()
    {
        playerBody = GetComponent<Rigidbody2D>();

        playerBody.bodyType = RigidbodyType2D.Static;
    }

    private void Update()
    {
        if (!IsDead && gameManager.IsGameActive)
        {
            bool pressedJumpButton = Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0);
            if (pressedJumpButton) Jump();

            transform.rotation = Quaternion.Lerp(Quaternion.Euler(transform.rotation.eulerAngles), fallingRotation, 0.05f);
        }
    }

    private void Jump()
    {
        playerBody.velocity = Vector2.zero;
        playerBody.AddForce(Vector2.up * JUMP_POWER);
        transform.rotation = Quaternion.Lerp(Quaternion.Euler(transform.rotation.eulerAngles), jumpRotation, 7f);

        SoundManager.Instance.Play(wingSound);
    }

    private void Die()
    {
        if (!IsDead)
        {
            SetPlayerActive(false);

            SoundManager.Instance.Play(hitSound);

            gameManager.GameOver();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Pipe") || collision.gameObject.CompareTag("Ground"))
        {
            Die();
        }

        else if (collision.gameObject.CompareTag("AddScoreArea"))
        {
            scoreManager.IncreaseCurrentScore();
        }
    }
}
