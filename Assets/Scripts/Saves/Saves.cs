using UnityEngine;

public static class Saves
{
    public static void ClearSaves()
    {
        PlayerPrefs.DeleteAll();
    }

    public static void Save()
    {
        PlayerPrefs.Save();
    }

    public static int GetCurrentScore() => PlayerPrefs.GetInt("currentScore", 0);

    public static void SaveCurrentScore(int value)
    {
        PlayerPrefs.SetInt("currentScore", value);
        Save();
    }

    public static int GetHighScore() => PlayerPrefs.GetInt("highScore", 0);

    public static void SaveHighScore(int value)
    {
        PlayerPrefs.SetInt("highScore", value);
        Save();
    }
}
